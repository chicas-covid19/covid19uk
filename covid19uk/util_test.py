"""Tests utility functions"""

from collections import OrderedDict
import tensorflow as tf
import numpy as np


def test_simple_bijection():

    from covid19uk.util import make_tensor_to_dict

    struct = OrderedDict(
        alpha=tf.constant([1.0, 2.0, 3.0]),
        beta=tf.constant(4.0),
    )

    bij = make_tensor_to_dict(struct)

    flat = bij.inverse(struct)
    struct_flat = bij(flat)

    np.testing.assert_array_equal(struct_flat, struct)


def test_matrix_bijection():

    from covid19uk.util import make_tensor_to_dict

    struct = OrderedDict(
        beta=tf.constant([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]]),
        alpha=tf.constant(7.0),
    )

    bij = make_tensor_to_dict(struct)

    flat = bij.inverse(struct)
    struct_flat = bij(flat)

    np.testing.assert_array_equal(struct_flat, struct)


def test_batch_bijection():
    from covid19uk.util import make_tensor_to_dict

    actual = OrderedDict(
        beta=tf.constant([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]]),
        alpha=tf.constant(7.0),
    )

    bij = make_tensor_to_dict(actual)

    flat = bij.inverse(actual)
    flat_batch = tf.stack([flat, flat], axis=0)
    required_batched = bij(flat_batch)

    actual_batched = OrderedDict(
        beta=tf.stack([actual["beta"], actual["beta"]], axis=0),
        alpha=tf.stack([actual["alpha"], actual["alpha"]], axis=0),
    )

    np.testing.assert_array_equal(
        required_batched["alpha"], actual_batched["alpha"]
    )
    np.testing.assert_array_equal(
        required_batched["beta"], actual_batched["beta"]
    )
