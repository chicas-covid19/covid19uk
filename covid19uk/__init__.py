"""Covid19UK model and associated inference/prediction algorithms"""

import tensorflow as tf

gpu_devices = tf.config.list_physical_devices("GPU")
if gpu_devices:
    tf.config.experimental.set_memory_growth(gpu_devices[0], True)

from covid19uk.data.assemble import assemble_data
from covid19uk.inference.inference import inference
from covid19uk.posterior.thin import thin_posterior
from covid19uk.posterior.reproduction_number import reproduction_number
from covid19uk.posterior.predict import predict
from covid19uk.posterior.within_between import within_between
from covid19uk.version import VERSION

__version__ = VERSION

__all__ = [
    "assemble_data",
    "inference",
    "thin_posterior",
    "reproduction_number",
    "predict",
    "within_between",
    "version",
]
