"""MCMC Test Rig for COVID-19 UK model"""
# pylint: disable=E402

import logging

import h5py
import xarray
import tqdm
import yaml
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp

from tensorflow_probability.python.internal import unnest
from tensorflow_probability.python.internal import samplers
from tensorflow_probability.python.experimental.stats import sample_stats

from gemlib.util import compute_state
from gemlib.mcmc import Posterior

from covid19uk.inference.mcmc import (
    StateStruct,
    ParamStruct,
    PARAM_BIJECTOR,
    fixed_window_kernel,
    step_size_adapting_kernel,
    full_adaptive_kernel,
)

import covid19uk.model_spec as model_spec

logging.basicConfig(format="[%(asctime)s] %(levelname)s %(message)s")
logger = logging.getLogger("covid19uk_inference")
logger.setLevel(logging.DEBUG)

# tf.config.run_functions_eagerly(True)
JIT_COMPILE = True
AUTOGRAPH = True

tfd = tfp.distributions
tfb = tfp.bijectors
DTYPE = model_spec.DTYPE


def _numpy_random_int(np_rng):
    return np_rng.integers(0, np.iinfo(np.int_).max, endpoint=False).astype(
        "int32"
    )


def _get_weighted_running_variance(draws, bijector=None):
    """Initialises online variance accumulator"""

    def running_variance(x):
        mean, variance = tf.nn.moments(x[-x.shape[0] // 2 :], axes=[0])
        return sample_stats.RunningVariance.from_stats(
            num_samples=tf.cast(x.shape[0] / 2, dtype=x.dtype),
            mean=mean,
            variance=variance,
        )

    if bijector is not None:
        draws = tf.nest.map_structure(
            lambda b, x: b.inverse(x), bijector, draws
        )

    return tf.nest.map_structure(running_variance, draws)


def _get_window_sizes(num_adaptation_steps):
    slow_window_size = num_adaptation_steps // 21
    first_window_size = 3 * slow_window_size
    last_window_size = (
        num_adaptation_steps - 15 * slow_window_size - first_window_size
    )
    return first_window_size, slow_window_size, last_window_size


def step_size_adaptive_sample(
    num_draws,
    joint_log_prob_fn,
    initial_position,
    hmc_kernel_kwargs,
    dual_averaging_kwargs,
    event_kernel_kwargs,
    trace_fn=None,
    seed=None,
):
    """
    In the fast adaptation window, we use the
    `DualAveragingStepSizeAdaptation` kernel
    to wrap an HMC kernel.

    :param num_draws: Number of MCMC draws in window
    :param joint_log_prob_fn: joint log posterior function
    :param initial_position: initial state of the Markov chain
    :param hmc_kernel_kwargs: `HamiltonianMonteCarlo` kernel keywords args
    :param dual_averaging_kwargs: `DualAveragingStepSizeAdaptation` keyword args
    :param event_kernel_kwargs: EventTimesMH and Occult kernel args
    :param trace_fn: function to trace kernel results
    :param seed: optional random seed.
    :returns: draws, kernel results, the adapted HMC step size, and variance
              accumulator
    """

    seed = samplers.sanitize_seed(seed, salt="_fast_adapt_window")

    kernel = step_size_adapting_kernel(
        joint_log_prob_fn=joint_log_prob_fn,
        hmc_kernel_kwargs=hmc_kernel_kwargs,
        dual_averaging_kwargs=dual_averaging_kwargs,
        event_kernel_kwargs=event_kernel_kwargs,
    )

    @tf.function(autograph=AUTOGRAPH, jit_compile=JIT_COMPILE)
    def sample(current_state, previous_kernel_results):
        return tfp.mcmc.sample_chain(
            num_draws,
            current_state=current_state,
            kernel=kernel,
            previous_kernel_results=previous_kernel_results,
            return_final_kernel_results=True,
            trace_fn=trace_fn,
            seed=seed,
        )

    pkr = kernel.bootstrap_results(initial_position)
    draws, trace, fkr = sample(initial_position, pkr)

    weighted_running_variance = _get_weighted_running_variance(
        ParamStruct(*[getattr(draws, s) for s in ParamStruct._fields]),
        PARAM_BIJECTOR,
    )

    step_size = unnest.get_outermost(fkr.inner_results[0], "step_size")

    return draws, trace, step_size, weighted_running_variance


def full_adaptive_sample(
    num_draws,
    joint_log_prob_fn,
    initial_position,
    initial_running_variance,
    hmc_kernel_kwargs,
    dual_averaging_kwargs,
    event_kernel_kwargs,
    trace_fn=None,
    seed=None,
):
    """In the slow adaptation phase, we adapt the HMC
    step size and mass matrix together.

    :param num_draws: number of MCMC iterations
    :param joint_log_prob_fn: the joint posterior density function
    :param initial_position: initial Markov chain state
    :param initial_running_variance: initial variance accumulator
    :param hmc_kernel_kwargs: `HamiltonianMonteCarlo` kernel kwargs
    :param dual_averaging_kwargs: `DualAveragingStepSizeAdaptation` kwargs
    :param event_kernel_kwargs: EventTimesMH and Occults kwargs
    :param trace_fn: result trace function
    :param seed: optional random seed
    :returns: draws, kernel results, adapted step size, the variance accumulator,
              and "learned" momentum distribution for the HMC.
    """

    seed = samplers.sanitize_seed(seed, salt="_slow_adapt_window")

    kernel = full_adaptive_kernel(
        joint_log_prob_fn=joint_log_prob_fn,
        hmc_initial_running_variance=initial_running_variance,
        hmc_kernel_kwargs=hmc_kernel_kwargs,
        dual_averaging_kwargs=dual_averaging_kwargs,
        event_kernel_kwargs=event_kernel_kwargs,
    )

    @tf.function(autograph=AUTOGRAPH, jit_compile=JIT_COMPILE)
    def sample(current_state, previous_kernel_results):
        return tfp.mcmc.sample_chain(
            num_draws,
            current_state=current_state,
            kernel=kernel,
            previous_kernel_results=previous_kernel_results,
            return_final_kernel_results=True,
            trace_fn=trace_fn,
            seed=seed,
        )

    pkr = kernel.bootstrap_results(initial_position)
    draws, trace, fkr = sample(initial_position, pkr)
    step_size = unnest.get_outermost(fkr.inner_results[0], "step_size")
    momentum_distribution = unnest.get_outermost(
        fkr.inner_results[0], "momentum_distribution"
    )

    weighted_running_variance = _get_weighted_running_variance(
        ParamStruct(*[getattr(draws, s) for s in ParamStruct._fields]),
        PARAM_BIJECTOR,
    )

    return (
        draws,
        trace,
        step_size,
        weighted_running_variance,
        momentum_distribution,
    )


def fixed_window_sampler(
    num_draws,
    joint_log_prob_fn,
    hmc_kernel_kwargs,
    event_kernel_kwargs,
    thin=1,
    trace_fn=None,
    jit_compile=False,
):
    """Fixed step size and mass matrix HMC.

    :param num_draws: number of MCMC iterations
    :param joint_log_prob_fn: joint log posterior density function
    :param initial_position: initial Markov chain state
    :param hmc_kernel_kwargs: `HamiltonianMonteCarlo` kwargs
    :param event_kernel_kwargs: Event and Occults kwargs
    :param trace_fn: results trace function
    :param seed: optional random seed
    :returns: (draws, trace, final_kernel_results)
    """
    kernel = fixed_window_kernel(
        joint_log_prob_fn=joint_log_prob_fn,
        hmc_kernel_kwargs=hmc_kernel_kwargs,
        event_kernel_kwargs=event_kernel_kwargs,
    )

    @tf.function(autograph=AUTOGRAPH, jit_compile=jit_compile)
    def sample_fn(current_state, previous_kernel_results=None, seed=None):

        seed = samplers.sanitize_seed(seed, salt="_fixed_sample")

        return tfp.mcmc.sample_chain(
            num_draws,
            current_state=current_state,
            kernel=kernel,
            return_final_kernel_results=True,
            previous_kernel_results=previous_kernel_results,
            num_steps_between_results=thin - 1,
            trace_fn=trace_fn,
            seed=seed,
        )

    return sample_fn, kernel


def trace_results_fn(_, results):
    """Packs results into a dictionary"""
    results_dict = {}
    root_results = results.inner_results

    step_size = tf.convert_to_tensor(
        unnest.get_outermost(root_results[0], "step_size")
    )

    results_dict["hmc"] = {
        "is_accepted": unnest.get_innermost(root_results[0], "is_accepted"),
        "target_log_prob": unnest.get_innermost(
            root_results[0], "target_log_prob"
        ),
        "step_size": step_size,
    }

    def get_move_results(results):
        return {
            "is_accepted": results.is_accepted,
            "target_log_prob": results.accepted_results.target_log_prob,
            "proposed_delta": tf.stack(
                [
                    results.accepted_results.m,
                    results.accepted_results.t,
                    results.accepted_results.delta_t,
                    results.accepted_results.x_star,
                ]
            ),
        }

    def get_initial_conditions_results(results):
        return {
            "is_accepted": results.is_accepted,
            "target_log_prob": results.accepted_results.target_log_prob,
            "proposed_delta": {
                "unit": results.accepted_results.unit,
                "timepoint": results.accepted_results.timepoint,
                "direction": results.accepted_results.direction,
                "num_events": results.accepted_results.num_events,
            },
        }

    res1 = root_results[1].inner_results
    results_dict["move/S->E"] = get_move_results(res1[0])
    results_dict["move/E->I"] = get_move_results(res1[1])
    results_dict["occult/S->E"] = get_move_results(res1[2])
    results_dict["occult/E->I"] = get_move_results(res1[3])
    results_dict["initial_conditions/S->E"] = get_initial_conditions_results(
        res1[4]
    )
    results_dict["initial_conditions/E->I"] = get_initial_conditions_results(
        res1[5]
    )

    return results_dict


def run_mcmc(
    joint_log_prob_fn,
    current_state,
    config,
    output_file,
    seed=None,
):

    seed = samplers.sanitize_seed(seed, salt="run_mcmc")

    first_window_size = 200
    last_window_size = 50
    slow_window_size = 25
    num_slow_windows = 10

    warmup_size = int(
        first_window_size
        + slow_window_size
        * ((1 - 2**num_slow_windows) / (1 - 2))  # sum geometric series
        + last_window_size
    )

    hmc_kernel_kwargs = {
        "step_size": 0.1,
        "num_leapfrog_steps": 16,
        "momentum_distribution": None,
        "store_parameters_in_results": True,
    }
    dual_averaging_kwargs = {
        "target_accept_prob": 0.75,
    }
    event_kernel_kwargs = {
        "stoichiometry": model_spec.STOICHIOMETRY,
        "config": config,
    }

    # Set up posterior
    logger.info("Initialising output...")
    sample_fn, _ = fixed_window_sampler(
        num_draws=1,
        joint_log_prob_fn=joint_log_prob_fn,
        hmc_kernel_kwargs=hmc_kernel_kwargs,
        event_kernel_kwargs=event_kernel_kwargs,
        trace_fn=trace_results_fn,
    )

    draws, trace, _ = sample_fn(current_state, seed=seed)
    posterior = Posterior(
        output_file,
        sample_dict=draws._asdict(),
        results_dict=trace,
        num_samples=warmup_size
        + config["num_burst_samples"] * config["num_bursts"],
    )
    offset = 0
    logger.info("Initial output complete")

    # Initial adaptive step-size sampling (parameters HMC only)
    logger.info(f"Fast window {first_window_size}")
    dual_averaging_kwargs["num_adaptation_steps"] = first_window_size
    kernel_seed, seed = samplers.split_seed(seed)
    draws, trace, step_size, running_variance = step_size_adaptive_sample(
        num_draws=first_window_size,
        joint_log_prob_fn=joint_log_prob_fn,
        initial_position=current_state,
        hmc_kernel_kwargs=hmc_kernel_kwargs,
        dual_averaging_kwargs=dual_averaging_kwargs,
        event_kernel_kwargs=event_kernel_kwargs,
        trace_fn=trace_results_fn,
        seed=kernel_seed,
    )
    current_state = tf.nest.map_structure(lambda x: x[-1], draws)
    posterior.write_samples(
        draws._asdict(),
        first_dim_offset=offset,
    )
    posterior.write_results(trace, first_dim_offset=offset)
    offset += first_window_size

    # Slow adaptation sampling
    hmc_kernel_kwargs["step_size"] = step_size
    for slow_window_idx in range(num_slow_windows):
        window_num_draws = int(slow_window_size * (1.5**slow_window_idx))
        dual_averaging_kwargs["num_adaptation_steps"] = window_num_draws
        kernel_seed, seed = samplers.split_seed(seed)
        logger.info(f"Slow window {window_num_draws}")
        (
            draws,
            trace,
            step_size,
            running_variance,
            momentum_distribution,
        ) = full_adaptive_sample(
            num_draws=window_num_draws,
            joint_log_prob_fn=joint_log_prob_fn,
            initial_position=current_state,
            initial_running_variance=running_variance,
            hmc_kernel_kwargs=hmc_kernel_kwargs,
            dual_averaging_kwargs=dual_averaging_kwargs,
            event_kernel_kwargs=event_kernel_kwargs,
            trace_fn=trace_results_fn,
            seed=kernel_seed,
        )
        hmc_kernel_kwargs["step_size"] = step_size
        hmc_kernel_kwargs["momentum_distribution"] = momentum_distribution
        current_state = tf.nest.map_structure(lambda s: s[-1], draws)
        posterior.write_samples(
            draws._asdict(),
            first_dim_offset=offset,
        )
        posterior.write_results(trace, first_dim_offset=offset)
        offset += window_num_draws

    # Fast adaptation sampling
    logger.info(f"Fast window {last_window_size}")
    dual_averaging_kwargs["num_adaptation_steps"] = last_window_size
    kernel_seed, seed = samplers.split_seed(seed)
    draws, trace, step_size, _ = step_size_adaptive_sample(
        num_draws=last_window_size,
        joint_log_prob_fn=joint_log_prob_fn,
        initial_position=current_state,
        hmc_kernel_kwargs=hmc_kernel_kwargs,
        dual_averaging_kwargs=dual_averaging_kwargs,
        event_kernel_kwargs=event_kernel_kwargs,
        trace_fn=trace_results_fn,
        seed=kernel_seed,
    )

    current_state = tf.nest.map_structure(lambda s: s[-1], draws)
    posterior.write_samples(
        draws._asdict(),
        first_dim_offset=offset,
    )
    posterior.write_results(trace, first_dim_offset=offset)
    offset += last_window_size
    hmc_kernel_kwargs["step_size"] = tf.reduce_mean(
        trace["hmc"]["step_size"][-last_window_size // 2 :]
    )

    # Fixed window sampling
    logger.info("Fixed window sampling begins")
    fixed_sample, kernel = fixed_window_sampler(
        config["num_burst_samples"],
        joint_log_prob_fn=joint_log_prob_fn,
        hmc_kernel_kwargs=hmc_kernel_kwargs,
        event_kernel_kwargs=event_kernel_kwargs,
        thin=config["thin"],
        trace_fn=trace_results_fn,
        jit_compile=JIT_COMPILE,
    )

    pkr = kernel.bootstrap_results(current_state)

    for i in tqdm.tqdm(
        range(config["num_bursts"]),
        unit_scale=config["num_burst_samples"] * config["thin"],
    ):
        kernel_seed, seed = samplers.split_seed(seed)
        draws, trace, pkr = fixed_sample(
            current_state,
            previous_kernel_results=pkr,
            seed=kernel_seed,
        )
        current_state = StateStruct(*[state_part[-1] for state_part in draws])
        posterior.write_samples(
            draws._asdict(),
            first_dim_offset=offset,
        )
        posterior.write_results(
            trace,
            first_dim_offset=offset,
        )
        offset += config["num_burst_samples"]

    logger.info("Fixed window sampling complete")

    return posterior


def initialise_chain(cases, data):
    """Initialises the MCMC chain from scratch, as opposed to
    with previous MCMC output"""

    events = model_spec.impute_censored_events(cases).numpy()

    # Initial conditions are calculated by calculating the state
    # at the beginning of the inference period
    #
    # Imputed censored events that pre-date the first I-R events
    # in the cases dataset are discarded.  They are only used to
    # to set up a sensible initial state.
    state = compute_state(
        initial_state=tf.concat(
            [
                tf.constant(data["N"], DTYPE)[:, tf.newaxis],
                tf.zeros_like(events[:, 0, :]),
            ],
            axis=-1,
        ),
        events=events,
        stoichiometry=model_spec.STOICHIOMETRY,
    )
    start_time = state.shape[1] - cases.shape[1]
    initial_state = state[:, start_time, :]
    events = events[:, start_time:-21, :]  # Clip off the "extra" events

    return StateStruct(
        alpha_0=tf.random.normal(
            (), mean=-0.65, stddev=0.3, dtype=DTYPE
        ),  # alpha_0
        psi=tf.math.exp(
            tf.random.normal((), mean=np.log(0.6), stddev=0.1, dtype=DTYPE)
        ),  # psi
        alpha_t=tf.random.normal(
            (events.shape[1] - 1,),
            mean=0.0,
            stddev=0.01,
            dtype=DTYPE,
        ),  # alpha_t
        sigma_space=tf.math.exp(
            tf.random.normal((), mean=np.log(0.05), stddev=0.3, dtype=DTYPE)
        ),  # sigma_space
        spatial_effect=tf.random.normal(
            (events.shape[0],),
            mean=0.0,
            stddev=0.1,
            dtype=DTYPE,
        ),  # spatial_effect
        gamma_1=tf.random.normal((), mean=0.4, stddev=0.1, dtype=DTYPE),
        initial_conditions=initial_state,
        seir=events,
    )


def inference(
    data_file, output_file, config, seed=None, use_autograph=False, use_xla=True
):
    """Constructs and runs the MCMC"""

    if tf.test.gpu_device_name():
        logger.info("Using GPU")
    else:
        logger.info("Using CPU")

    # Set up numpy, Eager, Graph Mode seeds (XLA below)
    np_rng = np.random.default_rng(seed)
    np_rand_seed = _numpy_random_int(np_rng)
    tf.random.set_seed(np_rand_seed)

    data = xarray.open_dataset(data_file, group="constant_data")
    cases = xarray.open_dataset(data_file, group="observations")[
        "cases"
    ].astype(DTYPE)
    dates = cases.coords["time"]

    # Impute censored events, return cases
    # Take the last week of data, and repeat a further 3 times
    # to get a better occult initialisation.
    extra_cases = tf.tile(cases[:, -7:], [1, 3])
    cases = tf.concat([cases, extra_cases], axis=-1)

    # Initialise the MCMC chain
    current_chain_state = initialise_chain(cases, data)

    # Instantiate the model
    model = model_spec.CovidUK(
        covariates=data,
        initial_step=0,
        num_steps=current_chain_state.seir.shape[-2],
    )
    logger.debug(
        f"Initial logpi: {model.log_prob(**current_chain_state._asdict())}"
    )

    # Run the MCMC
    posterior = run_mcmc(
        joint_log_prob_fn=model.log_prob,
        current_state=current_chain_state,
        config=config,
        output_file=output_file,
        seed=_numpy_random_int(np_rng),
    )

    # Store input dates in posterior
    posterior._file.create_dataset(
        "time",
        data=np.array(dates).astype(str).astype(h5py.string_dtype()),
    )

    # Log acceptances
    results_to_log = [
        "hmc",
        "move/S->E",
        "move/E->I",
        "occult/S->E",
        "occult/E->I",
        "initial_conditions/S->E",
        "initial_conditions/E->I",
    ]

    for r in results_to_log:
        logger.info(
            "Acceptance '%s': %0.2f",
            r,
            posterior[f"results/{r}/is_accepted"][:].mean(),
        )

    del posterior


if __name__ == "__main__":

    from argparse import ArgumentParser

    parser = ArgumentParser(description="Run MCMC inference algorithm")
    parser.add_argument(
        "-c", "--config", type=str, help="Config file", required=True
    )
    parser.add_argument(
        "-o", "--output", type=str, help="Output file", required=True
    )
    parser.add_argument(
        "-s", "--seed", type=int, help="Random seed", default=None
    )
    parser.add_argument("data_file", type=str, help="Data NetCDF file")

    args = parser.parse_args()

    with open(args.config, "r") as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    inference(args.data_file, args.output, config["Mcmc"])
