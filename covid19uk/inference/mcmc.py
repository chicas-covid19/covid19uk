"""Definitions of MCMC algorithm kernel functions"""

from collections import namedtuple

import tensorflow_probability as tfp
import tensorflow_probability.python.internal.dtype_util as dtype_util

from gemlib.mcmc import GibbsStep, GibbsKernel, MultiScanKernel
from covid19uk.inference.mcmc_kernel_factory import (
    transformed_kernel,
    hmc_kernel,
    hmc_full_adaptation_kernel,
    hmc_step_size_adaptation_kernel,
    partially_observed_events_kernel,
    occults_kernel,
    initial_conditions_kernel,
)

from covid19uk.model_spec import DTYPE

tfb = tfp.bijectors


# Data structures used to represent state
PARAM_NAMES = (
    "alpha_0",
    "psi",
    "alpha_t",
    "sigma_space",
    "spatial_effect",
    "gamma_1",
)

StateStruct = namedtuple(
    "StateStruct", PARAM_NAMES + ("initial_conditions", "seir")
)
ParamStruct = namedtuple("ParamStruct", PARAM_NAMES)


# The bijector transforms the parameter portion of the state
# to ensure measures are fully continuous (i.e. [-\infty, \infty]).
PARAM_BIJECTOR = ParamStruct(
    alpha_0=tfb.Identity(),
    psi=tfb.Softplus(low=dtype_util.eps(DTYPE)),
    alpha_t=tfb.Identity(),
    sigma_space=tfb.Softplus(low=dtype_util.eps(DTYPE)),
    spatial_effect=tfb.Identity(),
    gamma_1=tfb.Identity(),
)


def se_partially_observed_kernel(config):
    """Kernel to move partially observed S->E events.

    Args:
        config: a configuration dictionary.

    Returns:
        a function taking a target log probability function and
        state structure which itself returns an MCMC kernel.
    """
    return GibbsStep(
        "seir",
        lambda tlp_fn, state: partially_observed_events_kernel(
            target_log_prob_fn=tlp_fn,
            current_state=state,
            target_event_id=0,
            prev_event_id=None,
            next_event_id=1,
            config=config["move_kernel"]["se"],
            name="se_events",
        ),
    )


def ei_partially_observed_kernel(config):
    """Kernel to move partially observed E->I events.

    Args:
        config: a configuration dictionary.

    Returns:
        a function taking a target log probability function and
        state structure which itself returns an MCMC kernel.
    """
    return GibbsStep(
        "seir",
        lambda tlp_fn, state: partially_observed_events_kernel(
            target_log_prob_fn=tlp_fn,
            current_state=state,
            target_event_id=1,
            prev_event_id=0,
            next_event_id=2,
            config=config["move_kernel"]["ei"],
            name="ei_events",
        ),
    )


def se_occults_kernel(config):
    """Kernel to add/delete S->E occult events.

    Args:
        config: a configuration dictionary.

    Returns:
        a function taking a target log probability function and
        state structure which itself returns an MCMC kernel.
    """
    return GibbsStep(
        "seir",
        lambda tlp_fn, state: occults_kernel(
            target_log_prob_fn=tlp_fn,
            current_state=state,
            t_range=config["occult_kernel"]["se"]["t_range"],
            prev_event_id=None,
            target_event_id=0,
            next_event_id=1,
            config=config["occult_kernel"]["se"],
            name="se_occults",
        ),
    )


def ei_occults_kernel(config):
    """Kernel to add/delete E->I occult events.

    Args:
        config: a configuration dictionary.

    Returns:
        a function taking a target log probability function and
        state structure which itself returns an MCMC kernel.
    """
    return GibbsStep(
        "seir",
        lambda tlp_fn, state: occults_kernel(
            target_log_prob_fn=tlp_fn,
            current_state=state,
            t_range=config["occult_kernel"]["ei"]["t_range"],
            prev_event_id=0,
            target_event_id=1,
            next_event_id=2,
            config=config["occult_kernel"]["ei"],
            name="ei_occults",
        ),
    )


def se_initial_kernel(config, stoichiometry):
    """Kernel to update S->E initial events and state.

    Args:
        config: a configuration dictionary.
        stoichiometry: the epidemic stoichiometry matrix.

    Returns:
        a function taking a target log probability function and
        state structure which itself returns an MCMC kernel.
    """
    return GibbsStep(
        ("initial_conditions", "seir"),
        lambda tlp_fn, _: initial_conditions_kernel(
            target_log_prob_fn=tlp_fn,
            transition_index=0,
            stoichiometry=stoichiometry,
            max_timepoint=config["left_censored_kernel"]["se"]["max_timepoint"],
            max_events=config["left_censored_kernel"]["se"]["max_events"],
            name="se_initial",
        ),
    )


def ei_initial_kernel(config, stoichiometry):
    """Kernel to update E->I initial events and state.

    Args:
        config: a configuration dictionary.
        stoichiometry: the epidemic stoichiometry matrix.

    Returns:
        a function taking a target log probability function and
        state structure which itself returns an MCMC kernel.
    """
    return GibbsStep(
        ("initial_conditions", "seir"),
        lambda tlp_fn, _: initial_conditions_kernel(
            target_log_prob_fn=tlp_fn,
            transition_index=1,
            stoichiometry=stoichiometry,
            max_timepoint=config["left_censored_kernel"]["ei"]["max_timepoint"],
            max_events=config["left_censored_kernel"]["ei"]["max_events"],
            name="ei_initial",
        ),
    )


def event_compound_kernel(
    config,
    stoichiometry,
):
    """Invoke a compound kernel for events.

    Thie kernel invokes a chain of partially observed moves,
    occult add/del, and initial state moves multiple times.

    Args:
        config: a configuration dictionary.
        stoichiometry: the epidemic stoichiometry matrix.

    Returns:
        a function taking a target log probability function and
        state structure which itself returns an MCMC kernel.
    """

    def fn(target_log_prob_fn, state):
        return MultiScanKernel(
            config["num_event_time_updates"],
            GibbsKernel(
                target_log_prob_fn=target_log_prob_fn,
                kernel_list=[
                    se_partially_observed_kernel(config),
                    ei_partially_observed_kernel(config),
                    se_occults_kernel(config),
                    ei_occults_kernel(config),
                    se_initial_kernel(config, stoichiometry),
                    ei_initial_kernel(config, stoichiometry),
                ],
                name="multiscan_events_kernel",
            ),
        )

    return fn


def fixed_window_kernel(
    joint_log_prob_fn,
    hmc_kernel_kwargs,
    event_kernel_kwargs,
):
    """Create a compound sampler for fixed HMC and events

    Args:
        joint_log_prob_fn: the joint log probability function
        param_bijector: a _DefaultJointBijector to transform the parameters where
            required
        hmc_kernel_kwargs: a `dict` of arguments to...
        event_kernel_kwargs: a `dict` of arguments to...

    Returns:
        a compound MCMC kernel object
    """
    kernel_list = [
        GibbsStep(
            PARAM_NAMES,
            transformed_kernel(
                hmc_kernel(**hmc_kernel_kwargs),
                bijector=PARAM_BIJECTOR,
            ),
        ),
        GibbsStep(
            ("initial_conditions", "seir"),
            event_compound_kernel(
                config=event_kernel_kwargs["config"],
                stoichiometry=event_kernel_kwargs["stoichiometry"],
            ),
        ),
    ]

    return GibbsKernel(
        target_log_prob_fn=joint_log_prob_fn,
        kernel_list=kernel_list,
        name="fixed_window_gibbs0",
    )


def step_size_adapting_kernel(
    joint_log_prob_fn,
    hmc_kernel_kwargs,
    dual_averaging_kwargs,
    event_kernel_kwargs,
):
    """Create a compound sampler for the _fast adaptive_
       window of the HMC.

    Args:
        joint_log_prob_fn: the joint log probability function
        hmc_kernel_kwargs: arguments for the HMC kernel
        dual_averaging_kwargs: arguments for the Dual Averaging optimisation
        event_kernel_kwargs: arguments for the events kernel
    """
    kernel_list = [
        GibbsStep(
            PARAM_NAMES,
            transformed_kernel(
                hmc_step_size_adaptation_kernel(
                    hmc_kernel_kwargs, dual_averaging_kwargs
                ),
                bijector=PARAM_BIJECTOR,
            ),
        ),
        GibbsStep(
            ("initial_conditions", "seir"),
            event_compound_kernel(
                stoichiometry=event_kernel_kwargs["stoichiometry"],
                config=event_kernel_kwargs["config"],
            ),
        ),
    ]

    return GibbsKernel(
        target_log_prob_fn=joint_log_prob_fn,
        kernel_list=kernel_list,
        name="step_size_adapting",
    )


def full_adaptive_kernel(
    joint_log_prob_fn,
    hmc_initial_running_variance,
    hmc_kernel_kwargs,
    dual_averaging_kwargs,
    event_kernel_kwargs,
):
    """Fully adaptive compound kernel.

    Args:
        joint_log_prob_fn: joint log probability function
        hmc_kernel_kwargs: HMC arguments
        dual_averaging_kwargs: arguments for the Dual Averaging algorithm
        event_kernel_kwargs: arguments for the events kernel

    Returns:
        a GibbsKernel
    """
    kernel_list = [
        GibbsStep(
            PARAM_NAMES,
            transformed_kernel(
                hmc_full_adaptation_kernel(
                    hmc_initial_running_variance,
                    hmc_kernel_kwargs,
                    dual_averaging_kwargs,
                ),
                bijector=PARAM_BIJECTOR,
            ),
        ),
        GibbsStep(
            ("initial_conditions", "seir"),
            event_compound_kernel(
                stoichiometry=event_kernel_kwargs["stoichiometry"],
                config=event_kernel_kwargs["config"],
            ),
        ),
    ]

    return GibbsKernel(
        joint_log_prob_fn, kernel_list, name="full_adaptive_kernel"
    )
