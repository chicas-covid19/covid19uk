"""Generic MCMC kernel functions"""

from collections import namedtuple

import tensorflow_probability as tfp

from gemlib.mcmc import UncalibratedLeftCensoredEventTimesUpdate
from gemlib.mcmc import UncalibratedEventTimesUpdate
from gemlib.mcmc import UncalibratedOccultUpdate
from gemlib.mcmc import TransitionTopology
from gemlib.mcmc import MultiScanKernel
from gemlib.mcmc import GibbsKernel


# Build Metropolis within Gibbs sampler with windowed HMC
def hmc_kernel(
    step_size,
    num_leapfrog_steps,
    momentum_distribution,
    store_parameters_in_results,
):
    def fn(target_log_prob_fn, _):
        return tfp.experimental.mcmc.PreconditionedHamiltonianMonteCarlo(
            target_log_prob_fn=target_log_prob_fn,
            step_size=step_size,
            num_leapfrog_steps=num_leapfrog_steps,
            momentum_distribution=momentum_distribution,
            store_parameters_in_results=store_parameters_in_results,
        )

    return fn


def hmc_step_size_adaptation_kernel(
    hmc_kernel_kwargs,
    dual_averaging_kwargs,
):
    def fn(target_log_prob_fn, state):
        return tfp.mcmc.DualAveragingStepSizeAdaptation(
            hmc_kernel(
                **hmc_kernel_kwargs,
            )(target_log_prob_fn, state),
            **dual_averaging_kwargs,
        )

    return fn


def hmc_full_adaptation_kernel(
    initial_running_variance,
    hmc_kernel_kwargs,
    dual_averaging_kwargs,
):
    def fn(target_log_prob_fn, state):
        return tfp.experimental.mcmc.DiagonalMassMatrixAdaptation(
            hmc_step_size_adaptation_kernel(
                hmc_kernel_kwargs, dual_averaging_kwargs
            )(target_log_prob_fn, state),
            initial_running_variance=initial_running_variance,
        )

    return fn


def transformed_kernel(inner_kernel_fn, bijector):
    def fn(target_log_prob_fn, state):
        return tfp.mcmc.TransformedTransitionKernel(
            inner_kernel=inner_kernel_fn(target_log_prob_fn, state),
            bijector=bijector,
        )

    return fn


def partially_observed_events_kernel(
    target_log_prob_fn,
    current_state,
    target_event_id,
    prev_event_id,
    next_event_id,
    config,
    name=None,
):

    return tfp.mcmc.MetropolisHastings(
        inner_kernel=UncalibratedEventTimesUpdate(
            target_log_prob_fn=target_log_prob_fn,
            target_event_id=target_event_id,
            prev_event_id=prev_event_id,
            next_event_id=next_event_id,
            initial_state=current_state.initial_conditions,
            dmax=config["dmax"],
            mmax=config["m"],
            nmax=config["nmax"],
        ),
        name=name,
    )


def occults_kernel(
    target_log_prob_fn,
    current_state,
    t_range,
    prev_event_id,
    target_event_id,
    next_event_id,
    config,
    name,
):
    return tfp.mcmc.MetropolisHastings(
        inner_kernel=UncalibratedOccultUpdate(
            target_log_prob_fn=target_log_prob_fn,
            topology=TransitionTopology(
                prev_event_id, target_event_id, next_event_id
            ),
            cumulative_event_offset=current_state.initial_conditions,
            nmax=config["occult_nmax"],
            t_range=t_range,
            name=name,
        ),
        name=name,
    )


def initial_conditions_kernel(
    target_log_prob_fn,
    transition_index,
    stoichiometry,
    max_timepoint,
    max_events,
    name,
):
    return tfp.mcmc.MetropolisHastings(
        inner_kernel=UncalibratedLeftCensoredEventTimesUpdate(
            target_log_prob_fn,
            transition_index,
            stoichiometry,
            max_timepoint,
            max_events,
            name=name,
        ),
        name=name,
    )
